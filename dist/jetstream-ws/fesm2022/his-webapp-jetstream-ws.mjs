import * as i0 from '@angular/core';
import { Injectable } from '@angular/core';
import { connect, StringCodec } from 'nats.ws';

class JetStreamWsService {
    #nc;
    #jsm;
    #js;
    // 創建與 webSocket的連接 ws://0.0.0.0:8080
    async createConnection() {
        this.#nc = await connect(this.option.connectionOptions);
        if ((await this.setStreamConfig()) == '') {
            this.createConsumer(this.option.streamConfig?.name);
        }
        console.log(`connected to ${this.#nc.getServer()}`);
    }
    async setStreamConfig() {
        this.#jsm = await this.#nc.jetstreamManager();
        const { streamConfig } = this.option;
        const streams = await this.#jsm.streams.list().next();
        const stream = streams.find((stream) => stream.config.name === streamConfig?.name);
        if (stream) {
            const streamSubject = new Set([
                ...stream.config.subjects,
                ...streamConfig.subjects,
            ]);
            const streamInfo = await this.#jsm.streams.update(stream.config.name, {
                ...stream.config,
                ...streamConfig,
                subjects: [...streamSubject.keys()],
            });
            console.log(`Stream ${streamInfo.config.name} updated`);
            return stream.config.name;
        }
        else {
            console.log(`Stream ${streamConfig?.name} 不存在`);
            return '';
        }
    }
    async createStream() {
        const { streamConfig } = this.option;
        const streamInfo = await this.#jsm.streams.add(streamConfig);
        console.log(`Stream ${streamInfo.config.name} created`);
    }
    // create PullConsumer
    async createConsumer(stream) {
        try {
            this.#jsm = await this.#nc.jetstreamManager();
            const consumerInfo = this.#jsm.consumers.add(stream, this.option.consumerOptions);
            console.log(`create consumer ${(await consumerInfo).name} success`);
        }
        catch (err) {
            console.log(`創建失敗`);
        }
    }
    // 發送事件訊息
    async sendEventMessage(subject, event) {
        try {
            this.#js = this.#nc.jetstream();
            const sc = StringCodec();
            await this.#js.publish(subject, sc.encode(event));
            console.log(`發送成功`);
        }
        catch (err) {
            console.log(`發送失敗`);
        }
    }
    // 接收事件訊息
    async receiveEventMessage(stream, consumer) {
        this.#js = this.#nc.jetstream();
        const consumrApi = await this.#js.consumers.get(stream, consumer);
        const messages = await consumrApi.consume();
        try {
            for await (const message of messages) {
                console.log(message.seq);
                console.log(`consumer fetch: ${message.subject}`);
            }
        }
        catch (err) {
            console.log(`consume failed`);
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "16.2.0", ngImport: i0, type: JetStreamWsService, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "16.2.0", ngImport: i0, type: JetStreamWsService, providedIn: 'root' }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "16.2.0", ngImport: i0, type: JetStreamWsService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root',
                }]
        }] });

/*
 * Public API Surface of jetstream-ws
 */

/**
 * Generated bundle index. Do not edit.
 */

export { JetStreamWsService };
//# sourceMappingURL=his-webapp-jetstream-ws.mjs.map
