import { Injectable } from '@angular/core';
import { StringCodec, connect, } from 'nats.ws';
import * as i0 from "@angular/core";
export class JetStreamWsService {
    #nc;
    #jsm;
    #js;
    // 創建與 webSocket的連接 ws://0.0.0.0:8080
    async createConnection() {
        this.#nc = await connect(this.option.connectionOptions);
        if ((await this.setStreamConfig()) == '') {
            this.createConsumer(this.option.streamConfig?.name);
        }
        console.log(`connected to ${this.#nc.getServer()}`);
    }
    async setStreamConfig() {
        this.#jsm = await this.#nc.jetstreamManager();
        const { streamConfig } = this.option;
        const streams = await this.#jsm.streams.list().next();
        const stream = streams.find((stream) => stream.config.name === streamConfig?.name);
        if (stream) {
            const streamSubject = new Set([
                ...stream.config.subjects,
                ...streamConfig.subjects,
            ]);
            const streamInfo = await this.#jsm.streams.update(stream.config.name, {
                ...stream.config,
                ...streamConfig,
                subjects: [...streamSubject.keys()],
            });
            console.log(`Stream ${streamInfo.config.name} updated`);
            return stream.config.name;
        }
        else {
            console.log(`Stream ${streamConfig?.name} 不存在`);
            return '';
        }
    }
    async createStream() {
        const { streamConfig } = this.option;
        const streamInfo = await this.#jsm.streams.add(streamConfig);
        console.log(`Stream ${streamInfo.config.name} created`);
    }
    // create PullConsumer
    async createConsumer(stream) {
        try {
            this.#jsm = await this.#nc.jetstreamManager();
            const consumerInfo = this.#jsm.consumers.add(stream, this.option.consumerOptions);
            console.log(`create consumer ${(await consumerInfo).name} success`);
        }
        catch (err) {
            console.log(`創建失敗`);
        }
    }
    // 發送事件訊息
    async sendEventMessage(subject, event) {
        try {
            this.#js = this.#nc.jetstream();
            const sc = StringCodec();
            await this.#js.publish(subject, sc.encode(event));
            console.log(`發送成功`);
        }
        catch (err) {
            console.log(`發送失敗`);
        }
    }
    // 接收事件訊息
    async receiveEventMessage(stream, consumer) {
        this.#js = this.#nc.jetstream();
        const consumrApi = await this.#js.consumers.get(stream, consumer);
        const messages = await consumrApi.consume();
        try {
            for await (const message of messages) {
                console.log(message.seq);
                console.log(`consumer fetch: ${message.subject}`);
            }
        }
        catch (err) {
            console.log(`consume failed`);
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "16.2.0", ngImport: i0, type: JetStreamWsService, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "16.2.0", ngImport: i0, type: JetStreamWsService, providedIn: 'root' }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "16.2.0", ngImport: i0, type: JetStreamWsService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root',
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiamV0c3RyZWFtLXdzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9qZXRzdHJlYW0td3Mvc3JjL2xpYi9qZXRzdHJlYW0td3MvamV0c3RyZWFtLXdzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBSUwsV0FBVyxFQUNYLE9BQU8sR0FDUixNQUFNLFNBQVMsQ0FBQzs7QUFNakIsTUFBTSxPQUFPLGtCQUFrQjtJQUc3QixHQUFHLENBQWtCO0lBQ3JCLElBQUksQ0FBb0I7SUFDeEIsR0FBRyxDQUFtQjtJQUV0QixxQ0FBcUM7SUFDckMsS0FBSyxDQUFDLGdCQUFnQjtRQUNwQixJQUFJLENBQUMsR0FBRyxHQUFHLE1BQU0sT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxJQUFLLENBQUMsQ0FBQztTQUN0RDtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCxLQUFLLENBQUMsZUFBZTtRQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzlDLE1BQU0sRUFBRSxZQUFZLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBRXJDLE1BQU0sT0FBTyxHQUFHLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFdEQsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FDekIsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxLQUFLLFlBQVksRUFBRSxJQUFJLENBQ3RELENBQUM7UUFFRixJQUFJLE1BQU0sRUFBRTtZQUNWLE1BQU0sYUFBYSxHQUFHLElBQUksR0FBRyxDQUFDO2dCQUM1QixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUTtnQkFDekIsR0FBRyxZQUFhLENBQUMsUUFBUTthQUMxQixDQUFDLENBQUM7WUFFSCxNQUFNLFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRTtnQkFDcEUsR0FBRyxNQUFNLENBQUMsTUFBTTtnQkFDaEIsR0FBRyxZQUFZO2dCQUNmLFFBQVEsRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3BDLENBQUMsQ0FBQztZQUVILE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksVUFBVSxDQUFDLENBQUM7WUFDeEQsT0FBTyxNQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztTQUM1QjthQUFNO1lBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLFlBQVksRUFBRSxJQUFJLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELE9BQU8sRUFBRSxDQUFDO1NBQ1g7SUFDSCxDQUFDO0lBRUQsS0FBSyxDQUFDLFlBQVk7UUFDaEIsTUFBTSxFQUFFLFlBQVksRUFBRSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsTUFBTSxVQUFVLEdBQUcsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBYSxDQUFDLENBQUM7UUFDOUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxVQUFVLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUQsc0JBQXNCO0lBQ3RCLEtBQUssQ0FBQyxjQUFjLENBQUMsTUFBYztRQUNqQyxJQUFJO1lBQ0YsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUM5QyxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQzFDLE1BQU0sRUFDTixJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FDNUIsQ0FBQztZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsTUFBTSxZQUFZLENBQUMsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxDQUFDO1NBQ3JFO1FBQUMsT0FBTyxHQUFHLEVBQUU7WUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQztJQUNELFNBQVM7SUFDVCxLQUFLLENBQUMsZ0JBQWdCLENBQUMsT0FBZSxFQUFFLEtBQWE7UUFDbkQsSUFBSTtZQUNGLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUNoQyxNQUFNLEVBQUUsR0FBRyxXQUFXLEVBQUUsQ0FBQztZQUN6QixNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFFbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNyQjtRQUFDLE9BQU8sR0FBRyxFQUFFO1lBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNyQjtJQUNILENBQUM7SUFDRCxTQUFTO0lBQ1QsS0FBSyxDQUFDLG1CQUFtQixDQUFDLE1BQWMsRUFBRSxRQUFnQjtRQUN4RCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDaEMsTUFBTSxVQUFVLEdBQUcsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ2xFLE1BQU0sUUFBUSxHQUFHLE1BQU0sVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRTVDLElBQUk7WUFDRixJQUFJLEtBQUssRUFBRSxNQUFNLE9BQU8sSUFBSSxRQUFRLEVBQUU7Z0JBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQzthQUNuRDtTQUNGO1FBQUMsT0FBTyxHQUFHLEVBQUU7WUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDL0I7SUFDSCxDQUFDOzhHQTNGVSxrQkFBa0I7a0hBQWxCLGtCQUFrQixjQUZqQixNQUFNOzsyRkFFUCxrQkFBa0I7a0JBSDlCLFVBQVU7bUJBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQge1xuICBKZXRTdHJlYW1DbGllbnQsXG4gIEpldFN0cmVhbU1hbmFnZXIsXG4gIE5hdHNDb25uZWN0aW9uLFxuICBTdHJpbmdDb2RlYyxcbiAgY29ubmVjdCxcbn0gZnJvbSAnbmF0cy53cyc7XG5pbXBvcnQgeyBKZXRzdHJlYW1Xc09wdGlvbiB9IGZyb20gJy4vamV0c3RyZWFtLXdzLmludGVyZmFjZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnLFxufSlcbmV4cG9ydCBjbGFzcyBKZXRTdHJlYW1Xc1NlcnZpY2Uge1xuICBvcHRpb24hOiBKZXRzdHJlYW1Xc09wdGlvbjtcblxuICAjbmMhOiBOYXRzQ29ubmVjdGlvbjtcbiAgI2pzbSE6IEpldFN0cmVhbU1hbmFnZXI7XG4gICNqcyE6IEpldFN0cmVhbUNsaWVudDtcblxuICAvLyDlibXlu7roiIcgd2ViU29ja2V055qE6YCj5o6lIHdzOi8vMC4wLjAuMDo4MDgwXG4gIGFzeW5jIGNyZWF0ZUNvbm5lY3Rpb24oKSB7XG4gICAgdGhpcy4jbmMgPSBhd2FpdCBjb25uZWN0KHRoaXMub3B0aW9uLmNvbm5lY3Rpb25PcHRpb25zKTtcbiAgICBpZiAoKGF3YWl0IHRoaXMuc2V0U3RyZWFtQ29uZmlnKCkpID09ICcnKSB7XG4gICAgICB0aGlzLmNyZWF0ZUNvbnN1bWVyKHRoaXMub3B0aW9uLnN0cmVhbUNvbmZpZz8ubmFtZSEpO1xuICAgIH1cbiAgICBjb25zb2xlLmxvZyhgY29ubmVjdGVkIHRvICR7dGhpcy4jbmMuZ2V0U2VydmVyKCl9YCk7XG4gIH1cblxuICBhc3luYyBzZXRTdHJlYW1Db25maWcoKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICB0aGlzLiNqc20gPSBhd2FpdCB0aGlzLiNuYy5qZXRzdHJlYW1NYW5hZ2VyKCk7XG4gICAgY29uc3QgeyBzdHJlYW1Db25maWcgfSA9IHRoaXMub3B0aW9uO1xuXG4gICAgY29uc3Qgc3RyZWFtcyA9IGF3YWl0IHRoaXMuI2pzbS5zdHJlYW1zLmxpc3QoKS5uZXh0KCk7XG5cbiAgICBjb25zdCBzdHJlYW0gPSBzdHJlYW1zLmZpbmQoXG4gICAgICAoc3RyZWFtKSA9PiBzdHJlYW0uY29uZmlnLm5hbWUgPT09IHN0cmVhbUNvbmZpZz8ubmFtZSxcbiAgICApO1xuXG4gICAgaWYgKHN0cmVhbSkge1xuICAgICAgY29uc3Qgc3RyZWFtU3ViamVjdCA9IG5ldyBTZXQoW1xuICAgICAgICAuLi5zdHJlYW0uY29uZmlnLnN1YmplY3RzLFxuICAgICAgICAuLi5zdHJlYW1Db25maWchLnN1YmplY3RzLFxuICAgICAgXSk7XG5cbiAgICAgIGNvbnN0IHN0cmVhbUluZm8gPSBhd2FpdCB0aGlzLiNqc20uc3RyZWFtcy51cGRhdGUoc3RyZWFtLmNvbmZpZy5uYW1lLCB7XG4gICAgICAgIC4uLnN0cmVhbS5jb25maWcsXG4gICAgICAgIC4uLnN0cmVhbUNvbmZpZyxcbiAgICAgICAgc3ViamVjdHM6IFsuLi5zdHJlYW1TdWJqZWN0LmtleXMoKV0sXG4gICAgICB9KTtcblxuICAgICAgY29uc29sZS5sb2coYFN0cmVhbSAke3N0cmVhbUluZm8uY29uZmlnLm5hbWV9IHVwZGF0ZWRgKTtcbiAgICAgIHJldHVybiBzdHJlYW0hLmNvbmZpZy5uYW1lO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZyhgU3RyZWFtICR7c3RyZWFtQ29uZmlnPy5uYW1lfSDkuI3lrZjlnKhgKTtcbiAgICAgIHJldHVybiAnJztcbiAgICB9XG4gIH1cblxuICBhc3luYyBjcmVhdGVTdHJlYW0oKSB7XG4gICAgY29uc3QgeyBzdHJlYW1Db25maWcgfSA9IHRoaXMub3B0aW9uO1xuICAgIGNvbnN0IHN0cmVhbUluZm8gPSBhd2FpdCB0aGlzLiNqc20uc3RyZWFtcy5hZGQoc3RyZWFtQ29uZmlnISk7XG4gICAgY29uc29sZS5sb2coYFN0cmVhbSAke3N0cmVhbUluZm8uY29uZmlnLm5hbWV9IGNyZWF0ZWRgKTtcbiAgfVxuXG4gIC8vIGNyZWF0ZSBQdWxsQ29uc3VtZXJcbiAgYXN5bmMgY3JlYXRlQ29uc3VtZXIoc3RyZWFtOiBzdHJpbmcpIHtcbiAgICB0cnkge1xuICAgICAgdGhpcy4janNtID0gYXdhaXQgdGhpcy4jbmMuamV0c3RyZWFtTWFuYWdlcigpO1xuICAgICAgY29uc3QgY29uc3VtZXJJbmZvID0gdGhpcy4janNtLmNvbnN1bWVycy5hZGQoXG4gICAgICAgIHN0cmVhbSxcbiAgICAgICAgdGhpcy5vcHRpb24uY29uc3VtZXJPcHRpb25zLFxuICAgICAgKTtcbiAgICAgIGNvbnNvbGUubG9nKGBjcmVhdGUgY29uc3VtZXIgJHsoYXdhaXQgY29uc3VtZXJJbmZvKS5uYW1lfSBzdWNjZXNzYCk7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBjb25zb2xlLmxvZyhg5Ym15bu65aSx5pWXYCk7XG4gICAgfVxuICB9XG4gIC8vIOeZvOmAgeS6i+S7tuioiuaBr1xuICBhc3luYyBzZW5kRXZlbnRNZXNzYWdlKHN1YmplY3Q6IHN0cmluZywgZXZlbnQ6IHN0cmluZykge1xuICAgIHRyeSB7XG4gICAgICB0aGlzLiNqcyA9IHRoaXMuI25jLmpldHN0cmVhbSgpO1xuICAgICAgY29uc3Qgc2MgPSBTdHJpbmdDb2RlYygpO1xuICAgICAgYXdhaXQgdGhpcy4janMucHVibGlzaChzdWJqZWN0LCBzYy5lbmNvZGUoZXZlbnQpKTtcblxuICAgICAgY29uc29sZS5sb2coYOeZvOmAgeaIkOWKn2ApO1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgY29uc29sZS5sb2coYOeZvOmAgeWkseaVl2ApO1xuICAgIH1cbiAgfVxuICAvLyDmjqXmlLbkuovku7boqIrmga9cbiAgYXN5bmMgcmVjZWl2ZUV2ZW50TWVzc2FnZShzdHJlYW06IHN0cmluZywgY29uc3VtZXI6IHN0cmluZykge1xuICAgIHRoaXMuI2pzID0gdGhpcy4jbmMuamV0c3RyZWFtKCk7XG4gICAgY29uc3QgY29uc3VtckFwaSA9IGF3YWl0IHRoaXMuI2pzLmNvbnN1bWVycy5nZXQoc3RyZWFtLCBjb25zdW1lcik7XG4gICAgY29uc3QgbWVzc2FnZXMgPSBhd2FpdCBjb25zdW1yQXBpLmNvbnN1bWUoKTtcblxuICAgIHRyeSB7XG4gICAgICBmb3IgYXdhaXQgKGNvbnN0IG1lc3NhZ2Ugb2YgbWVzc2FnZXMpIHtcbiAgICAgICAgY29uc29sZS5sb2cobWVzc2FnZS5zZXEpO1xuICAgICAgICBjb25zb2xlLmxvZyhgY29uc3VtZXIgZmV0Y2g6ICR7bWVzc2FnZS5zdWJqZWN0fWApO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgY29uc29sZS5sb2coYGNvbnN1bWUgZmFpbGVkYCk7XG4gICAgfVxuICB9XG59XG4iXX0=