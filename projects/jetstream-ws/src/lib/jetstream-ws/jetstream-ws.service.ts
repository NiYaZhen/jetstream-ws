import { Injectable } from '@angular/core';

import {
  JetStreamClient,
  JetStreamManager,
  NatsConnection,
  StringCodec,
  connect,
} from 'nats.ws';
import { JetstreamWsOption } from './jetstream-ws.interface';

@Injectable({
  providedIn: 'root',
})
export class JetStreamWsService {
  option!: JetstreamWsOption;

  #nc!: NatsConnection;
  #jsm!: JetStreamManager;
  #js!: JetStreamClient;

  // 創建與 webSocket的連接 ws://0.0.0.0:8080
  async createConnection() {
    this.#nc = await connect(this.option.connectionOptions);
    if ((await this.setStreamConfig()) == '') {
      this.createConsumer(this.option.streamConfig?.name!);
    }
    console.log(`connected to ${this.#nc.getServer()}`);
  }

  async setStreamConfig(): Promise<string> {
    this.#jsm = await this.#nc.jetstreamManager();
    const { streamConfig } = this.option;

    const streams = await this.#jsm.streams.list().next();

    const stream = streams.find(
      (stream) => stream.config.name === streamConfig?.name,
    );

    if (stream) {
      const streamSubject = new Set([
        ...stream.config.subjects,
        ...streamConfig!.subjects,
      ]);

      const streamInfo = await this.#jsm.streams.update(stream.config.name, {
        ...stream.config,
        ...streamConfig,
        subjects: [...streamSubject.keys()],
      });

      console.log(`Stream ${streamInfo.config.name} updated`);
      return stream!.config.name;
    } else {
      console.log(`Stream ${streamConfig?.name} 不存在`);
      return '';
    }
  }

  async createStream() {
    const { streamConfig } = this.option;
    const streamInfo = await this.#jsm.streams.add(streamConfig!);
    console.log(`Stream ${streamInfo.config.name} created`);
  }

  // create PullConsumer
  async createConsumer(stream: string) {
    try {
      this.#jsm = await this.#nc.jetstreamManager();
      const consumerInfo = this.#jsm.consumers.add(
        stream,
        this.option.consumerOptions,
      );
      console.log(`create consumer ${(await consumerInfo).name} success`);
    } catch (err) {
      console.log(`創建失敗`);
    }
  }
  // 發送事件訊息
  async sendEventMessage(subject: string, event: string) {
    try {
      this.#js = this.#nc.jetstream();
      const sc = StringCodec();
      await this.#js.publish(subject, sc.encode(event));

      console.log(`發送成功`);
    } catch (err) {
      console.log(`發送失敗`);
    }
  }
  // 接收事件訊息
  async receiveEventMessage(stream: string, consumer: string) {
    this.#js = this.#nc.jetstream();
    const consumrApi = await this.#js.consumers.get(stream, consumer);
    const messages = await consumrApi.consume();

    try {
      for await (const message of messages) {
        console.log(message.seq);
        console.log(`consumer fetch: ${message.subject}`);
      }
    } catch (err) {
      console.log(`consume failed`);
    }
  }
}
