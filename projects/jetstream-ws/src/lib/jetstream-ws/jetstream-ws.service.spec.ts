import { TestBed } from '@angular/core/testing';
import { JetStreamWsService } from './jetstream-ws.service';

describe('NatsClientService', () => {
  let service: JetStreamWsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JetStreamWsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
